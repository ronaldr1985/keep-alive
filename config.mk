# The compiler
CC = clang

# Path
PREFIX = /usr/local

X11INC = /usr/X11R6/include
X11LIB = /usr/X11R6/lib

# Includes and libs
INCS = -I${X11INC}
LIBS = -lX11

# compiler flags:
#  -g    adds debugging information to the executable file
#  -Wall turns on most, but not all, compiler warnings
CFLAGS  = -g -Wall -std=c99 -pedantic $(INCS) $(LIBS)
