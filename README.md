# Keep Alive

A simple C program that stops the screen from going to sleep by moving the mouse by a pixel and then by moving it back.

## Usage

Currently Keep Alive has two arguments that are listed below:

**-h**, **--help**:	Prints a help page

**-s**, **--seconds**:	Changes the interval between the mouse moves

## Install 

It's a single executable, just add it to your path, obviously after making sure that it doesn't do anything nasty, which it doesn't, if you really must check then the C code is in the repository.

## Compiling, or editing the source code

The source code is pretty simple all contained in one file, the program uses the XDO header files to move the mouse. That project can be found here: https://github.com/baskerville/xdo
