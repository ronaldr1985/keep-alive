#include "keep-alive.h"

void write_help(void)
{
	printf("A simple program to stop the screen from sleeping.\nIf no arguments are passed to the script the program shall run until stopped by user input, KeepAlive stops the screen from sleeping by moving the mouse by a pixel and then moving it back again\n\nUsage:\n  -h, --help: Displays this very help page\n  -s, --seconds: Changes the interval between the mouse movements\n");
}

int stay_awake(int secs)
{
	// Define a bunch of variables for later
	int tmp, new_x, new_y, position_x, position_y = 0;
	unsigned int mask = 0;
	Window child_win, root_win;

	// Setup display and such
	char *display_name = getenv("DISPLAY");
	if (!display_name) {
	    fprintf(stderr, "Cannot connect to X server '%s'\n", display_name);
	    exit(1);
	}
	Display *display = XOpenDisplay(display_name);
	int screen = DefaultScreen(display);
	//Get system window
	Window root_window;
	root_window = XRootWindow(display, 0);

	for(;;) {
		// Get the mouse cursor position
		XQueryPointer(display, XRootWindow(display, screen), &child_win, &root_win, &tmp, &tmp, &position_x, &position_y, &mask);
		// Where the mouse should move temporarily
		new_y = position_y + 1;
		new_x = position_x + 1;
		// Move the mouse
		XSelectInput(display, root_window, KeyReleaseMask);
		XWarpPointer(display, None, root_window, 0, 0, 0, 0, new_x, new_y);
		XWarpPointer(display, None, root_window, 0, 0, 0, 0, position_x, position_y);
		XFlush(display);
		sleep(secs);
	}

	XCloseDisplay(display);

	return 0;
}

int main(int argc, char *argv[])
{
	int secs = 20;

	printf("\e[1;1H\e[2J");

	if(argc >= 2) {
		if(strcmp(argv[1], "--help")==0 || strcmp(argv[1], "-h") == 0) {
			write_help();
		}
		else if(strcmp(argv[1], "--seconds")==0 || strcmp(argv[1], "-s") == 0) {
			secs = atoi(argv[2]);
			stay_awake(secs);
		}
	} else {
		stay_awake(secs);
	}
	return 0;
}


