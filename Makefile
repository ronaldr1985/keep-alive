include config.mk

NAME=keep-alive

# the build target executable:
TARGET=keep-alive

all: options $(TARGET)

$(TARGET): src/$(TARGET).c
	$(CC) -o $(TARGET) src/$(TARGET).c $(CFLAGS)

options:
	@echo ${NAME} build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "CC       = ${CC}"

clean:
	rm -f $(TARGET)

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f $(TARGET) ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/$(TARGET)

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/$(TARGET)

PHONY: all options clean install uninstall
